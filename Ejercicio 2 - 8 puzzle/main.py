from search_algorithms import *

def main():
    estado_final = (1, 2, 3, 4, 5, 6, 7, 8, 0)
    estado_inicial = (8, 7, 5, 3, 0, 1, 4, 2, 6)

    #Menú principal
    print("Este programa encuentra la solución al 8-puzzle")
    print("El estado inicial del juego es: ")
    (Nodo(estado_inicial, None, None, 0, calcular_heurisitica(estado_inicial))).imprimir_nodo()
    print("\n¿Qué algoritmo desea correr? Escriba:")
    print("\t\"bfs\" para correr Breadth First Search")
    print("\t\"dfs\" para correr Depth First Search")
    print("\tCualquier otra cosa para terminar el programa.")
    algoritmo = input("Su elección: ")

    #Selección de algoritmo
    if algoritmo == "bfs" or algoritmo == "BFS":
        print("Corriendo BFS. Por favor espere.")
        nodos_camino = bfs(estado_inicial, estado_final)

    elif algoritmo == "dfs" or algoritmo == "DFS":
        print("\n¿Establecer un límite de profundidad?")
        print("Escriba el límite con un entero mayor que 0")
        print("o cero para continuar sin límite.")
        profundidad_max = int(input("Profundidad: "))
        print("Corriendo DFS. Por favor espere.")
        nodos_camino = dfs(estado_inicial, estado_final, profundidad_max)

    else:
        return 0

    #Se imprime el camino si existe y si el usuario lo desea.
    if nodos_camino:
        print("El camino tiene", len(nodos_camino), "movimientos.")
        imprimir_camino = (input("¿Desea imprimir dicho camino? s/n: "))

        if imprimir_camino == "s" or imprimir_camino == "S":
            print("\nEstado inicial:")
            (Nodo(estado_inicial, None, None, 0, calcular_heurisitica(estado_inicial))).imprimir_nodo()
            print("Piezas correctas:", calcular_heurisitica(estado_inicial), "\n")
            input("Presione \"enter\" para continuar.")

            for nodo in nodos_camino:
                print("\nSiguiente movimiento:", nodo.movimiento)
                print("Estado actual:")
                nodo.imprimir_nodo()
                print("Piezas correctas:", nodo.piezas_correctas, "\n")
                input("Presione \"enter\" para continuar.")
    else:
        print("\nNo se encontró un camino con las condiciones dadas.")


if __name__ == "__main__":
    main()
