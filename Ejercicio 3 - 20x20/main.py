from search_algorithms import bfs, dfs
import random

# funcion para generar tablero de 20x20
def generar_tablero():
    matriz = []
    for i in range(20):
        matriz.append([])
        for j in range(20):
            matriz[i].append((i, j))

    return matriz

def main():
    estado_final = (random.randint(0, 19), random.randint(0, 19))
    estado_inicial = (random.randint(0, 19), random.randint(0, 19))
    tablero = generar_tablero()

    #Menú principal
    print("Este programa encuentra la ruta en el espacio 20x20.")
    print("Punto de Inicio: ({}, {})".format(estado_inicial[0], estado_inicial[1]))
    print("Punto de Meta: ({}, {})".format(estado_final[0], estado_final[1]))
    print("\n¿Qué algoritmo desea correr? Escriba:")
    print("\t\"bfs\" para correr Breadth First Search")
    print("\t\"dfs\" para correr Depth First Search")
    print("\tCualquier otra cosa para terminar el programa.")
    algoritmo = input("Su elección: ")

    #Selección de algoritmo
    if algoritmo == "bfs" or algoritmo == "BFS":
        print("Corriendo BFS. Por favor espere.")
        nodos_camino = bfs(estado_inicial, estado_final)

    elif algoritmo == "dfs" or algoritmo == "DFS":
        print("Corriendo DFS. Por favor espere.")
        nodos_camino = dfs(estado_inicial, estado_final)

    else:
        return 0
    
    # Se imprime el camino en el tablero si existe
    if nodos_camino:
        nodos_camino.insert(0, estado_inicial)
        print(nodos_camino)
        print('--------------------------------------------------')
        contador = 0

        for i in tablero:
            fila = 1
            if contador < 10:
                print(contador, " |", end=" ")
            else:
                print(contador, "|", end=" ")

            for j in i:
                if j in nodos_camino:
                    if j == estado_inicial:
                        print("A", end=" ")

                    elif j == estado_final:
                        print("B", end=" ")

                    else:
                        print("x", end=" ")
                else:
                    print("*", end=" ")

                if fila == 20:
                    print()
                fila += 1
                
            contador += 1

    else:
        print("\nNo se encontró un camino con las condiciones dadas.")
    
    input("\nPresione \"enter\" para continuar.")

if __name__ == "__main__":
    main()
