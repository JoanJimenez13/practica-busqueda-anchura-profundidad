#Clase que define un nodo en el 8-puzzle..
class Nodo:
    def __init__(self, estado, padre, movimiento, profundidad):
        self.estado = estado
        self.padre = padre
        self.movimiento = movimiento
        self.profundidad = profundidad

    #Método para mover las piezas en direcciones posibles.
    def mover(self, direccion):
        fila = self.estado[0]
        columna = self.estado[1]

        if direccion == "arriba":
            if fila != 0:
                fila = fila - 1
                return tuple([fila, columna])
            else:
                return None

        elif direccion == "abajo":
            if fila != 19:
                fila = fila + 1
                return tuple([fila, columna])
            else:
                return None

        elif direccion == "derecha":
            if columna != 19:
                columna = columna + 1
                return tuple([fila, columna])
            else:
                return None

        elif direccion == "izquierda":
            if columna != 0:
                columna = columna - 1
                return tuple([fila, columna])
            else:
                return None

    #Método que encuentra y regresa todos los nodos sucesores del nodo actual.
    def encontrar_sucesores(self):
        sucesores = []
        sucesorN = self.mover("arriba")
        sucesorS = self.mover("abajo")
        sucesorE = self.mover("derecha")
        sucesorO = self.mover("izquierda")

        sucesores.append(Nodo(sucesorN, self, "arriba", self.profundidad + 1))
        sucesores.append(Nodo(sucesorS, self, "abajo", self.profundidad + 1))
        sucesores.append(Nodo(sucesorE, self, "derecha", self.profundidad + 1))
        sucesores.append(Nodo(sucesorO, self, "izquierda", self.profundidad + 1))

        sucesores = [nodo for nodo in sucesores if nodo.estado != None]
        return sucesores

    #Método que encuentra el camino desde el nodo inicial hasta el actual.
    def encontrar_camino(self):
        camino = []
        nodo_actual = self
        while nodo_actual.profundidad >= 1:
            camino.append(nodo_actual.estado)
            nodo_actual = nodo_actual.padre
        camino.reverse()
        return camino

    #Método que imprime ordenadamente el estado (piezas) de un nodo.
    def imprimir_nodo(self):
        print("Punto: ({}, {})".format(self.estado[0], self.estado[1]))
